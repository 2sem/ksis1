﻿using System;
using System.IO;
using System.Net;

namespace Network
{
    public static class BinaryEndPoint
    {
        private static int AddressOffset = 4;
        private static int ArrayBeginning = 0;
        private static void AddToStream(Byte[] bytes, MemoryStream memoryStream)
        {
            memoryStream.Write(bytes, ArrayBeginning, bytes.Length);
        }
        public static Byte[] SerializeEndPoint(IPEndPoint endPoint)
        {
            using(var memoryStream = new MemoryStream())
            {
                var portBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(endPoint.Port));
                AddToStream(portBytes, memoryStream);
                var addressBytes = endPoint.Address.GetAddressBytes();
                AddToStream(addressBytes, memoryStream);
                return memoryStream.ToArray();
            }
        }
        public static IPEndPoint DeserializeEndPoint(Byte[] bytes, int size)
        {
            using (var memoryStream = new MemoryStream())
            {
                var port = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, ArrayBeginning));
                var addressLength = size - AddressOffset;
                var address = new byte[addressLength];
                Array.Copy(bytes, AddressOffset, address, ArrayBeginning, addressLength);
                return new IPEndPoint(new IPAddress(address), port);
                
            }
        }
    }
}
