﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Network
{
    public static class Sockets
    {
        public static Socket createUdpSocket(IPEndPoint endPoint, bool enableBroadcast)
        {
            var udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            udpSocket.EnableBroadcast = enableBroadcast;
            udpSocket.Bind(endPoint);
            return udpSocket;
        }
        public static IPAddress getLocalIPv4Address()
        {
            IPHostEntry hostEntry = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress address in hostEntry.AddressList)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    return address;
                }
            }
            return null;
        }
    }
}
