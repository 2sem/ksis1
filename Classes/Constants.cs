﻿namespace Network
{
    public static class Constants
    {
        public static ushort udpBroadcastPort = 6000;
        public static int udpBufferSize = 1024;
        public static int udpTimeout = 1000;
    }
}
