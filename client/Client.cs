﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using static Network.BinaryEndPoint;
using static Network.Constants;
using static Network.Sockets;

namespace Client
{
    class Client
    {
        private static int clientUdpPort = new Random().Next(IPEndPoint.MaxPort);

        private static byte[] udpReceiveBuffer = new byte[udpBufferSize];

        private static readonly Socket tcpServerSocket = new Socket
            (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


        static void Main(string[] args)
        {
            StartUdp();

        }

        private static void StartUdp()
        { 
            var clientUdpEndPoint = new IPEndPoint(getLocalIPv4Address(), clientUdpPort);
            var udpClientSocket = createUdpSocket(clientUdpEndPoint, true);
            udpClientSocket.ReceiveTimeout = udpTimeout;
            Console.WriteLine("Starting client");
            while (true)
            { 
                IPEndPoint broadcastEndPoint = new IPEndPoint(IPAddress.Broadcast, udpBroadcastPort);
                udpClientSocket.SendTo(SerializeEndPoint(clientUdpEndPoint), broadcastEndPoint);

                IPEndPoint senderIPEndPoint = new IPEndPoint(IPAddress.None, 0);
                EndPoint senderEndPoint = (EndPoint)senderIPEndPoint;
                int bytesCount = udpClientSocket.ReceiveFrom(udpReceiveBuffer, ref senderEndPoint);
                var serverEndPoint = DeserializeEndPoint(udpReceiveBuffer, bytesCount);
                Console.WriteLine(senderIPEndPoint.Address + ": " + serverEndPoint.Address.ToString() + ":" + serverEndPoint.Port);


                Console.ReadLine();
            }
        }
    }
}
