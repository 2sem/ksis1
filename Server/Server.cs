﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using static Network.BinaryEndPoint;
using static Network.Constants;
using static Network.Sockets;

namespace Server
{
    class Server
    {
        private static int bufferSize = 1024;

        private static byte[] receiveBuffer = new byte[bufferSize];

        private static readonly Socket tcpServerSocket = new Socket
            (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


        static void Main(string[] args)
        {
            StartUdp();

        }

        private static void StartUdp()
        {
            var serverUdpEndPoint = new IPEndPoint(getLocalIPv4Address(), udpBroadcastPort);
            var udpServerSocket = createUdpSocket(serverUdpEndPoint, true);
            IPEndPoint senderIPEndPoint = new IPEndPoint(IPAddress.None, 0);
            EndPoint senderEndPoint = (EndPoint)senderIPEndPoint;
            Console.WriteLine("Starting server...");
            while (true)
            {
                int bytes = udpServerSocket.ReceiveFrom(receiveBuffer, ref senderEndPoint);
                var clientEndPoint = DeserializeEndPoint(receiveBuffer, bytes);
                Console.WriteLine(senderIPEndPoint.Address + ": " + clientEndPoint.Address.ToString() + ":" + clientEndPoint.Port);
                udpServerSocket.SendTo(SerializeEndPoint(serverUdpEndPoint), clientEndPoint);
            }



        }
    }
}
